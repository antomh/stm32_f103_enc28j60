/*
 * net.h
 *
 *  Created on: Dec 15, 2020
 *  Author: Anton Shein<anton-shein2008@yandex.ru>
 *  ----------------------------------------------------------------------
 *  Copyright (C) Anton Shein, 2020
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ----------------------------------------------------------------------
 */

#ifndef INC_NET_H_
#define INC_NET_H_


#include "stm32f1xx_hal.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "enc28j60.h"


/* Change 16-bit to big-endian format */
#define reverse_byte_in_word(x) ( (((x)>>8) & 0xff) | (((x)<<8) & 0xff00) )
#define ETH_ARP                 reverse_byte_in_word(0x0806)
#define ETH_IP                  reverse_byte_in_word(0x0800)
#define ARP_ETH                 reverse_byte_in_word(0x0001)
#define ARP_IP                  reverse_byte_in_word(0x0800)
#define ARP_REQUEST             reverse_byte_in_word(1)
#define ARP_REPLY               reverse_byte_in_word(2)
#define IP_ADDR                 {192, 168, 11, 197}


/* Frame structure */
typedef struct enc28j60_frame
{
    uint8_t     addr_dest[6];
    uint8_t     addr_src[6];
    uint16_t    type;
    uint8_t     data[];
} enc28j60_frame_ptr;

typedef struct arp_msg
{
    uint16_t net_tp;
    uint16_t proto_tp;
    uint8_t macaddr_len;
    uint8_t ipaddr_len;
    uint16_t op;
    uint8_t macaddr_src[6];
    uint8_t ipaddr_src[4];
    uint8_t macaddr_dst[6];
    uint8_t ipaddr_dst[4];
} arp_msg_ptr;


/* Init external functions */
void net_init(void);
void arp_send(enc28j60_frame_ptr* frame);
uint8_t arp_read(enc28j60_frame_ptr* frame, uint16_t len);
void eth_send(enc28j60_frame_ptr* frame, uint16_t len);
void eth_read(enc28j60_frame_ptr* frame, uint16_t len);
void net_poll(void);


#endif /* INC_NET_H_ */
