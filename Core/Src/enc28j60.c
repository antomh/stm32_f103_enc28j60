/*
 * enc28j60.c
 *
 *  Created on: Dec 15, 2020
 *  Author: Anton Shein<anton-shein2008@yandex.ru>
 *  ----------------------------------------------------------------------
 *  Copyright (C) Anton Shein, 2020
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ----------------------------------------------------------------------
 */

#include "enc28j60.h"
/*-------------------------------------------------------*/



/* Init variables */
extern SPI_HandleTypeDef hspi1;         // init SPI1 settings struct
static uint8_t Enc28j60Bank;            // number of current memory bank
static int gNextPacketPtr;              // transmit packet inception pointer
uint8_t macaddr[6] = MAC_ADDR;          // set MAC address massive



/**
 * @brief  Error handler
 * @note   Switch on LED (at PC13)
 * @param  None
 * @retval None
 */
static void Error(void)
{
    LD_ON;
}
/*-------------------------------------------------------*/


/**
 * @brief  SPI write/read function
 * @note   Use to write or read byte of data to or from SPI device
 * @param  byte: byte to transmit
 * @retval Received byte from SPI device
 */
static uint8_t SPIx_WriteRead(uint8_t byte)
{
    uint8_t byteRecieved = 0;
    if ( HAL_SPI_TransmitReceive(&hspi1, (uint8_t*)&byte, (uint8_t*)&byteRecieved, 1, 0x1000) != HAL_OK )
    {
        Error();
    }
    return byteRecieved;
}
/*-------------------------------------------------------*/


/**
 * @brief  SPI send byte function
 * @param  byte: byte to transmit by SPI
 * @retval None
 */
static void SPI_SendByte(uint8_t byte)
{
    SPIx_WriteRead(byte);
}
/*-------------------------------------------------------*/


/**
 * @brief  SPI receive byte function
 * @param  None
 * @retval Received byte from SPI device
 */
static uint8_t SPI_RecieveByte(void)
{
    uint8_t byte = SPIx_WriteRead(0xFF);
    return byte;
}
/*-------------------------------------------------------*/


/**
 * @brief  Write byte to ENC28J60 register
 * @note   For more information about operation instructions go to ENC28J60 datasheet page 26
 * @param  op: what write operation do we do
 * @param  addr: what the address we write to
 * @param  data: what data do we write
 * @retval None
 */
static void enc28j60_writeOp(uint8_t op, uint8_t addr, uint8_t data)
{
    /* Choose this SPI device */
    RESET_CS_PIN;
    /* Send what write operation do we do */
    SPI_SendByte( op | ( addr & ADDR_MASK ) );
    /* Send data */
    SPI_SendByte( data );
    /* Unleash this SPI device */
    SET_CS_PIN;
}
/*-------------------------------------------------------*/


/**
 * @brief  Read byte from ENC28J60 register
 * @note   For more information about operation instructions go to ENC28J60 datasheet page 26
 * @param  op: what read operation do we do
 * @param  addr: what the address we read
 * @retval Getting byte from ENC28J60
 */
static uint8_t enc28j60_readOp(uint8_t op, uint8_t addr)
{
    uint8_t result;
    /* Choose this SPI device */
    RESET_CS_PIN;
    /* Send what read operation do we do */
    SPI_SendByte( op | ( addr & ADDR_MASK ) );
    /* Pass dummy byte (datasheet, page 27) */
    SPI_SendByte(0x00);
    if ( addr & 0x80 ) SPI_RecieveByte();
    /* Receive byte */
    result = SPI_RecieveByte();
    /* Unleash this SPI device */
    SET_CS_PIN;

    return result;
}
/*-------------------------------------------------------*/


/**
 * @brief  Write ENC28J60 buffer
 * @param  len: data length to write
 * @param  data: pointer to the data to be write
 * @retval None
 */
static void enc28j60_writeBuf(uint16_t len, uint8_t* data)
{
    RESET_CS_PIN;
    SPI_SendByte(ENC28J60_WRITE_BUF_MEM);
    while (len--)
    {
        SPI_SendByte(*data++);
    }
    SET_CS_PIN;
}
/*-------------------------------------------------------*/


/**
 * @brief  Read ENC28J60 buffer
 * @param  len: length buffer to read
 * @param  data: pointer to the data array where buffer will be copy
 * @retval None
 */
static void enc28j60_readBuf(uint16_t len, uint8_t* data)
{
    RESET_CS_PIN;
    SPI_SendByte(ENC28J60_READ_BUF_MEM);
    while (len--)
    {
        *data++ = SPIx_WriteRead(0x00);
    }
    SET_CS_PIN;
}
/*-------------------------------------------------------*/


/**
 * @brief  Set current memory bank
 * @note   Call it before @ref enc28j60_writeOp()
 * @param  addr: variable address we need to write to / read from
 * @retval None
 */
static void enc28j60_setBank(uint8_t addr)
{
    if ( ( addr & BANK_MASK ) != Enc28j60Bank )
    {
        enc28j60_writeOp( ENC28J60_BIT_FIELD_CLR, ECON1, ( ECON1_BSEL1 | ECON1_BSEL0 ) );   // clear BSEL1 and BSEL0
        Enc28j60Bank = addr & BANK_MASK;                                                    // put bank number
        enc28j60_writeOp( ENC28J60_BIT_FIELD_SET, ECON1, ( Enc28j60Bank >> 5 ) );           // shift bank number
    }
}
/*-------------------------------------------------------*/


/**
 * @brief  Write common control registers
 * @param  addr: variable address we need to write to
 * @param  data: data to be write
 * @retval None
 */
static void enc28j60_writeRegByte(uint8_t addr, uint8_t data)
{
    enc28j60_setBank(addr);
    enc28j60_writeOp( ENC28J60_WRITE_CTRL_REG, addr, data);
}
//---------------------------------------------------------


/**
 * @brief  Read common control registers
 * @param  addr: variable address we need to read from
 * @retval None
 */
static uint8_t enc28j60_readRegByte(uint8_t addr)
{
    enc28j60_setBank(addr);
    return enc28j60_readOp( ENC28J60_READ_CTRL_REG, addr);
}
//---------------------------------------------------------


/**
 * @brief  Write 2-byte registers
 * @param  addr: variable address we need to write to
 * @param  data: data to be write
 * @retval None
 */
static void enc28j60_writeReg(uint8_t addr, uint16_t data)
{
    enc28j60_writeRegByte(addr, data);
    enc28j60_writeRegByte(addr + 1, data >> 8);
}
//---------------------------------------------------------


/**
 * @brief  Write PHY registers
 * @param  addr: variable address we need to write to
 * @param  data: data to be write
 * @retval None
 */
static void enc28j60_writePhy(uint8_t addr, uint16_t data)
{
    enc28j60_writeRegByte(MIREGADR, addr);                  // send address PHY register to MIREGADR
    enc28j60_writeReg(MIWR, data);                          // send data we need to write to PHY to MIWR
    while ( enc28j60_readRegByte(MISTAT) & MISTAT_BUSY );   // wait until BUSY is high
}
/*-------------------------------------------------------*/


/**
 * @brief  Module ENC28J60 initialization function
 * @note   Call it before any other ENC28J60 function
 * @param  None
 * @retval None
 */
void enc28j60_init(void)
{
    LD_OFF;
    /* Soft reset */
    enc28j60_writeOp(ENC28J60_SOFT_RESET, 0, ENC28J60_SOFT_RESET);
    /* Wait until reload is done */
    HAL_Delay(2);
    /* Check everything is ok */
    while ( !enc28j60_readOp( ENC28J60_READ_CTRL_REG, ESTAT ) & ESTAT_CLKRDY );

    /* setup buffers */
    enc28j60_writeReg(ERXST,RXSTART_INIT);
    enc28j60_writeReg(ERXRDPT,RXSTART_INIT);
    enc28j60_writeReg(ERXND,RXSTOP_INIT);
    enc28j60_writeReg(ETXST,TXSTART_INIT);
    enc28j60_writeReg(ETXND,TXSTOP_INIT);

    /* enable broadcast */
    enc28j60_writeRegByte(ERXFCON, enc28j60_readRegByte( ERXFCON | ERXFCON_BCEN ) );

    /* setup channel level */
    enc28j60_writeRegByte(MACON1, ( MACON1_MARXEN | MACON1_TXPAUS | MACON1_RXPAUS ) );
    enc28j60_writeRegByte(MACON2, 0x00);
    enc28j60_writeOp(ENC28J60_BIT_FIELD_SET, MACON3, ( MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN ) );
    enc28j60_writeReg(MAIPG, 0x0C12);
    enc28j60_writeRegByte(MABBIPG, 0x12);                       // ���������� ����� ��������
    enc28j60_writeReg(MAMXFL,MAX_FRAMELEN);                     // ������������ ������ ������
    enc28j60_writeRegByte(MAADR5, macaddr[0]);                  // set MAC address
    enc28j60_writeRegByte(MAADR4, macaddr[1]);
    enc28j60_writeRegByte(MAADR3, macaddr[2]);
    enc28j60_writeRegByte(MAADR2, macaddr[3]);
    enc28j60_writeRegByte(MAADR1, macaddr[4]);
    enc28j60_writeRegByte(MAADR0, macaddr[5]);

    /* setup physical level of initialization */
    enc28j60_writePhy(PHCON2,PHCON2_HDLDIS);                    // ��������� loopback
    enc28j60_writePhy(  PHLCON, PHLCON_LACFG2 |                 // ����������
                        PHLCON_LBCFG2 | PHLCON_LBCFG1 | PHLCON_LBCFG0 |
                        PHLCON_LFRQ0 | PHLCON_STRCH );
    enc28j60_setBank(ECON1);
    enc28j60_writeOp(ENC28J60_BIT_FIELD_SET,EIE,EIE_INTIE|EIE_PKTIE);
    enc28j60_writeOp(ENC28J60_BIT_FIELD_SET,ECON1,ECON1_RXEN);  // ��������� ���� �������
}
//---------------------------------------------------------





/**
 * @brief  Receive ethernet packet
 * @note
 * @param  *buf: start buffer address
 * @param  buflen: data length to send
 * @retval None
 */
uint16_t enc28j60_packetReceive(uint8_t* buf, uint16_t buflen)
{
    uint16_t len = 0;                                           // ���������� ��� �������� ����� ������ � ������������ ��� ��������
    if ( enc28j60_readRegByte(EPKTCNT) > 0 )                    // ���� ���������� �������� ������� > 0 �� ������� ��
    {
        enc28j60_writeReg(ERDPT, gNextPacketPtr);

        struct                                                  // ��������� ��������� ��� �������� ���������
        {
            uint16_t nextPacket;
            uint16_t byteCount;
            uint16_t status;
        } header;

        enc28j60_readBuf(sizeof(header), (uint8_t*)&header);   // ��������� ��������� ������

        gNextPacketPtr = header.nextPacket;                     // ���������� ��������� �� ��������� ����� � ����������
        len = header.byteCount - 4;                             // ���������� ����� ������ (-4 ����� ����������� ����� (CRC))

        if ( len > buflen ) len = buflen;                       // ���� ����� ��������� �������� �������� (buflen), �� ��������� �

        if ( ( header.status & 0x80 ) == 0 )                    // ��������� ������
        {
            len = 0;
        }
        else
        {
            enc28j60_readBuf(len, buf);                         // ���� ��� �� - ������ �����
        }
        buf[len] = 0;                                           // ������� � ��������� ���� ������������ ������ 0

        if ( (gNextPacketPtr - 1) > RXSTOP_INIT )
        {
            enc28j60_writeReg(ERXRDPT, RXSTOP_INIT);
        }
        else
        {
            enc28j60_writeReg(ERXRDPT, (gNextPacketPtr - 1) );
        }

        enc28j60_writeOp(ENC28J60_BIT_FIELD_SET, ECON2, ECON2_PKTDEC);
    }
    return len;                                                 // ���������� ����� ��������� ������
}
//---------------------------------------------------------


/**
 * @brief  Send ethernet packet
 * @note
 * @param  *buf: start buffer address
 * @param  buflen: data length to send
 * @retval None
 */
void enc28j60_packetSend(uint8_t* buf, uint16_t buflen)
{
    while ( enc28j60_readOp(ENC28J60_READ_CTRL_REG, ECON1) & ECON1_TXRTS )
    {
        if ( enc28j60_readRegByte(EIR) & EIR_TXERIF )
        {
            enc28j60_writeOp(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRST);
            enc28j60_writeOp(ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRST);
        }
    }

    /* some debug code*/
    LD_TOGGLE;
//    char temp_str[20];
//    sprintf(temp_str,"\r--debug marker--\n");
//    HAL_UART_Transmit( &huart1, (uint8_t*)temp_str, strlen(temp_str), 0x1000 );

    enc28j60_writeReg(EWRPT, TXSTART_INIT);
    enc28j60_writeReg(EWRPT, TXSTART_INIT + buflen);
    uint8_t zero_to_send = 0x00;
    enc28j60_writeBuf(1, &zero_to_send);
    enc28j60_writeBuf(buflen, buf);
    enc28j60_writeOp(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRTS);
}
//---------------------------------------------------------
