/*
 * net.c
 *
 *  Created on: Dec 15, 2020
 *  Author: Anton Shein<anton-shein2008@yandex.ru>
 *  ----------------------------------------------------------------------
 *  Copyright (C) Anton Shein, 2020
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ----------------------------------------------------------------------
 */


#include "net.h"
#include "stdio.h"


/* Init variables */
uint8_t net_buf[ENC28J60_MAXFRAME];
uint8_t ipaddr[4] = IP_ADDR;
char str1[60] = {0};
uint8_t err_cnt = 0;

extern UART_HandleTypeDef huart1;
extern uint8_t macaddr[6];


/**
 * @brief  Init network settings
 * @note   Call it before any other NETWORK function
 * @param  None
 * @retval None
 */
void net_init(void)
{
    enc28j60_init();
}
//---------------------------------------------------------


/**
 * @brief  Read ARP packet
 * @note
 * @param  *frame
 * @param  len
 * @retval 1, if ENC28J60 knows its adress while request
 */
uint8_t arp_read(enc28j60_frame_ptr* frame, uint16_t len)
{
    uint8_t res = 0;
    arp_msg_ptr* msg = (void*)(frame->data);        // ���������� ��������� �� �������������� ���

    if ( len >= sizeof(arp_msg_ptr) )
    {
        if ( (msg->net_tp == ARP_ETH) && (msg->proto_tp == ARP_IP) )
        {
            if ( (msg->op == ARP_REQUEST) && ( !memcmp(msg->ipaddr_dst, ipaddr, 4) ) )
            {
                sprintf(str1,"\r Request\n\rmac_src %02X:%02X:%02X:%02X:%02X:%02X\n",
                        msg->macaddr_src[0], msg->macaddr_src[1], msg->macaddr_src[2],
                        msg->macaddr_src[3], msg->macaddr_src[4], msg->macaddr_src[5]);
                HAL_UART_Transmit(&huart1, (uint8_t*)str1, strlen(str1), 0x1000);

                sprintf(str1,"\r ip_src %d.%d.%d.%d\n",
                        msg->ipaddr_src[0], msg->ipaddr_src[1],
                        msg->ipaddr_src[2], msg->ipaddr_src[3]);
                HAL_UART_Transmit(&huart1, (uint8_t*)str1, strlen(str1), 0x1000);

                sprintf(str1,"\r mac_dst %02X:%02X:%02X:%02X:%02X:%02X\n",
                        msg->macaddr_dst[0], msg->macaddr_dst[1], msg->macaddr_dst[2],
                        msg->macaddr_dst[3], msg->macaddr_dst[4], msg->macaddr_dst[5]);
                HAL_UART_Transmit(&huart1, (uint8_t*)str1, strlen(str1), 0x1000);

                sprintf(str1,"\r ip_dst %d.%d.%d.%d\n",
                        msg->ipaddr_dst[0],msg->ipaddr_dst[1],
                        msg->ipaddr_dst[2],msg->ipaddr_dst[3]);
                HAL_UART_Transmit(&huart1, (uint8_t*)str1, strlen(str1), 0x1000);

                res=1;
            }
        }
    }

    return res;
}
//---------------------------------------------------------


/**
 * @brief  Send frame through ethernet
 * @param  *frame:
 * @param  len:
 * @retval None
 */
void eth_send(enc28j60_frame_ptr* frame, uint16_t len)
{
    memcpy( frame->addr_dest, frame->addr_src, 6 );
    memcpy( frame->addr_src, macaddr, 6 );

    enc28j60_packetSend( (void*)frame, len + sizeof(enc28j60_frame_ptr) );
}
//---------------------------------------------------------


/**
 * @brief  Send ARP packet
 * @note
 * @param  *frame
 * @retval None
 */
void arp_send(enc28j60_frame_ptr* frame)
{
    arp_msg_ptr* msg = (void*) frame->data;

    msg->op = ARP_REPLY;
    memcpy( msg->macaddr_dst, msg->macaddr_src, 6 );
    memcpy( msg->macaddr_src, macaddr, 6 );
    memcpy( msg->ipaddr_dst, msg->ipaddr_src, 4 );
    memcpy( msg->ipaddr_src, ipaddr, 4 );

    eth_send(frame, sizeof(arp_msg_ptr));
}
//---------------------------------------------------------


/**
 * @brief  Read frame (data link layer)
 * @param  *frame:
 * @param  len:
 * @retval None
 */
void eth_read(enc28j60_frame_ptr* frame, uint16_t len)
{
    /* some debug code*/
    char temp_str2[30];
    sprintf(temp_str2,"\rinside eth_read() :\n");
    HAL_UART_Transmit( &huart1, (uint8_t*)temp_str2, strlen(temp_str2), 0x1000 );

    if ( len >= sizeof( enc28j60_frame_ptr ) )
    {
        if ( frame->type == ETH_ARP )
        {
            /* some debug code*/
//            char temp_str[40];
//            sprintf(temp_str,"\t\t\tinside if (frame is ARP) :\n");
//            HAL_UART_Transmit( &huart1, (uint8_t*)temp_str, strlen(temp_str), 0x1000 );

            sprintf(str1,"\r%02X:%02X:%02X:%02X:%02X:%02X - %02X:%02X:%02X:%02X:%02X:%02X; %d; arp\n",
                            frame->addr_src[0],frame->addr_src[1],frame->addr_src[2],frame->addr_src[3],frame->addr_src[4],frame->addr_src[5],
                            frame->addr_dest[0],frame->addr_dest[1],frame->addr_dest[2],frame->addr_dest[3],frame->addr_dest[4],frame->addr_dest[5],
                            len);
            HAL_UART_Transmit( &huart1, (uint8_t*)str1, strlen(str1), 0x1000 );

//            arp_read(frame, len - sizeof(enc28j60_frame_ptr));

            if ( arp_read(frame, len - sizeof(enc28j60_frame_ptr) ) )
            {
                arp_send(frame);
            }
        }
        else if ( frame->type == ETH_IP )
        {
            sprintf(str1,"\r%02X:%02X:%02X:%02X:%02X:%02X - %02X:%02X:%02X:%02X:%02X:%02X; %d; ip\n",
                            frame->addr_src[0],frame->addr_src[1],frame->addr_src[2],frame->addr_src[3],frame->addr_src[4],frame->addr_src[5],
                            frame->addr_dest[0],frame->addr_dest[1],frame->addr_dest[2],frame->addr_dest[3],frame->addr_dest[4],frame->addr_dest[5],
                            len);
            HAL_UART_Transmit( &huart1, (uint8_t*)str1, strlen(str1), 0x1000 );
        }
        else
        {
            sprintf(str1,"\rOooops...\n");
            HAL_UART_Transmit( &huart1, (uint8_t*)str1, strlen(str1), 0x1000 );
        }
    }
}
//---------------------------------------------------------


/**
 * @brief  Network polling
 * @param  None
 * @retval None
 */
void net_poll(void)
{
    uint16_t len;
    enc28j60_frame_ptr* frame = (void*) net_buf;

    /* some debug code*/
//    char temp_str[30];
//    sprintf(temp_str,"inside net_poll() :\n");
//    HAL_UART_Transmit( &huart1, (uint8_t*)temp_str, strlen(temp_str), 0x1000 );

    while ( ( len = enc28j60_packetReceive( net_buf, sizeof(net_buf) ) ) > 0 )
    {
        eth_read(frame, len);
    }
}
//---------------------------------------------------------
